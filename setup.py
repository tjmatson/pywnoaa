#!/usr/bin/env python

from setuptools import setup, find_packages
# from pywnoaa import __version__

with open('README.md') as f:
    long_description = f.read()

setup(
    name='pywnoaa',
    license='MIT',
    url='https://gitlab.com/tjmatson/pywnoaa',
    author='Timothy Matson',
    version='1.0',
    packages=['pywnoaa'],
    scripts=['pywnoaa/pywnoaa'],
    test_suite='pywnoaa.test.test_pywnoaa',
    description='Format and display NOAA weather data.',
    long_description=long_description,
    classifiers=[
        'Programming Language :: Python',
        'Development Status :: 5',
        'Intended Audience :: End Users/Desktop',
        'Environment :: Console',
        'Natural Language :: English',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)
