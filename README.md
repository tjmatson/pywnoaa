Pywnoaa
=========

Format and display NOAA weather data.

    pywnoaa station [OPTIONS] [FORMAT]

Installation
------------

    git clone git@gitlab.com:tjmatson/pywnoaa.git
    cd pywnoaa
    python setup.py install

Quickstart
----------

Find ICAO station codes: http://w1.weather.gov/xml/current\_obs/

    $ pywnoaa KJFK
    Fair | 23.3C
    $ pywnoaa KJFK "Current temperature is %cF"
    Current temperature is 74.0F

Options
-------

    -h --help                   Show help message and exit.
    -f --fahrenheit             Display default weather message in fahrenheit.
    -p --print-error            Print NOAA errors instead of raising them. Mainly for display purposes.
    -v --version                Display version info and exit.

Format Options
--------------

    %a    weather                 (e.g., Partly Cloudy)
    %b    temp_c                  (28.9)
    %c    temp_f                  (84.0)
    %d    temperature_string      (84.0 F (28.9 C))
    %e    dewpoint_c              (24.7)
    %f    dewpoint_f              (76.5)
    %g    dewpoint_string         (76.5 F (24.7 C))
    %h    heat_index_c            (29)
    %i    heat_index_f            (84)
    %j    heat_index_string       (84 F (29 C))
    %k    windchill_c             (15)
    %l    windchill_f             (59)
    %m    windchill_string        (59 F (15 C))
    %n    wind_degrees            (20)
    %o    wind_dir                (North)
    %p    wind_gust_kt            (16)
    %q    wind_gust_mph           (0.0)
    %r    wind_kt                 (7.97)
    %s    wind_mph                (9.2)
    %t    wind_string             (North at 9.2 MPH ( 7.97 KT))
    %u    pressure_in             (30.24)
    %v    pressure_mb             (1022.0)
    %w    pressure_string         (1022.0 mb)
    %x    pressure_tendency_mb    (-0.2)
    %y    suggested_pickup        (15 minutes after the hour)
    %z    suggested_pickup_period (60)
    %A    relative_humidity       (56)
    %B    visibility_mi           (10.00)
    %C    water_temp_c            (28.9)
    %D    water_temp_f            (84.0)
    %E    water_column_height     (5.125)
    %F    mean_wave_degrees       (281)
    %G    mean_wave_dir           (North)
    %H    wave_height_m           (0.98)
    %I    wave_height_ft          (0.3)
    %J    tide_ft                 (1.67)
    %K    average_period_sec      (6.5)
    %L    dominant_period_sec     (10)
    %M    observation_time        (Last Updated on Sep 23 2015, 200 am AST)
    %N    observation_time_rfc822 (Wed, 23 Sep 2015 020000 -0400)
    %O    latitude                (36.43333)
    %P    longitude               (-81.41667)
    %Q    location                (New York, Kennedy International Airport, NY)
    %R    station_id              (KJFK)
    %%    a literal %
