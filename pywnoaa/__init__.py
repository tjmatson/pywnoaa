from .api import (
    main,
    noaa_weather,
    map_data,
    format_string
)

"""
Format and display NOAA weather data.

:copyright: (c) 2015 by Timothy Matson
:license: MIT
"""

__title__ = 'pywnoaa'
__version__ = '1.0'
__author__ = 'Timothy Matson'
__copyright__ = 'Copyright 2015 Timothy Matson'
__license__ = 'MIT'
