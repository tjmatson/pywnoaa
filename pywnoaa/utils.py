from argparse import RawTextHelpFormatter, ArgumentParser


NOAA_FIELDS = {'%a': 'weather',
               '%b': 'temp_c',
               '%c': 'temp_f',
               '%d': 'temperature_string',
               '%e': 'dewpoint_c',
               '%f': 'dewpoint_f',
               '%g': 'dewpoint_string',
               '%h': 'heat_index_c',
               '%i': 'heat_index_f',
               '%j': 'heat_index_string',
               '%k': 'windchill_c',
               '%l': 'windchill_f',
               '%m': 'windchill_string',
               '%n': 'wind_degrees',
               '%o': 'wind_dir',
               '%p': 'wind_gust_kt',
               '%q': 'wind_gust_mph',
               '%r': 'wind_kt',
               '%s': 'wind_mph',
               '%t': 'wind_string',
               '%u': 'pressure_in',
               '%v': 'pressure_mb',
               '%w': 'pressure_string',
               '%x': 'pressure_tendency_mb',
               '%y': 'suggested_pickup',
               '%z': 'suggested_pickup_period',
               '%A': 'relative_humidity',
               '%B': 'visibility_mi',
               '%C': 'water_temp_c',
               '%D': 'water_temp_f',
               '%E': 'water_column_height',
               '%F': 'mean_wave_degrees',
               '%G': 'mean_wave_dir',
               '%H': 'wave_height_m',
               '%I': 'wave_height_ft',
               '%J': 'tide_ft',
               '%K': 'average_period_sec',
               '%L': 'dominant_period_sec',
               '%M': 'observation_time',
               '%N': 'observation_time_rfc822',
               '%O': 'latitude',
               '%P': 'longitude',
               '%Q': 'location',
               '%R': 'station_id',
               }


def parse_args():
    argparser = ArgumentParser(description='Display current weather information.',
                               formatter_class=RawTextHelpFormatter)

    argparser.add_argument('station', help='station ICAO code (e.g. KJFK) \
(see for instance http://w1.weather.gov/xml/current_obs/seek.php)')

    argparser.add_argument('-f', '--fahrenheit',
                           help='display temperature as fahrenheit',
                           action='store_const', const='%a | %cF')

    argparser.add_argument('-p', '--print-error',
                           help='print NOAA errors instead of raising them',
                           action='store_true')

    argparser.add_argument('-v', '--version', action='version',
                           version='1.0')

    argparser.add_argument('format', nargs='?', help='''weather \
output format\n
Interpreted sequences are:\n
%%a    weather                 (e.g., Partly Cloudy)
%%b    temp_c                  (28.9)
%%c    temp_f                  (84.0)
%%d    temperature_string      (84.0 F (28.9 C))
%%e    dewpoint_c              (24.7)
%%f    dewpoint_f              (76.5)
%%g    dewpoint_string         (76.5 F (24.7 C))
%%h    heat_index_c            (29)
%%i    heat_index_f            (84)
%%j    heat_index_string       (84 F (29 C))
%%k    windchill_c             (15)
%%l    windchill_f             (59)
%%m    windchill_string        (59 F (15 C))
%%n    wind_degrees            (20)
%%o    wind_dir                (North)
%%p    wind_gust_kt            (16)
%%q    wind_gust_mph           (0.0)
%%r    wind_kt                 (7.97)
%%s    wind_mph                (9.2)
%%t    wind_string             (North at 9.2 MPH ( 7.97 KT))
%%u    pressure_in             (30.24)
%%v    pressure_mb             (1022.0)
%%w    pressure_string         (1022.0 mb)
%%x    pressure_tendency_mb    (-0.2)
%%y    suggested_pickup        (15 minutes after the hour)
%%z    suggested_pickup_period (60)
%%A    relative_humidity       (56)
%%B    visibility_mi           (10.00)
%%C    water_temp_c            (28.9)
%%D    water_temp_f            (84.0)
%%E    water_column_height     (5.125)
%%F    mean_wave_degrees       (281)
%%G    mean_wave_dir           (North)
%%H    wave_height_m           (0.98)
%%I    wave_height_ft          (0.3)
%%J    tide_ft                 (1.67)
%%K    average_period_sec      (6.5)
%%L    dominant_period_sec     (10)
%%M    observation_time        (Last Updated on Sep 23 2015, 200 am AST)
%%N    observation_time_rfc822 (Wed, 23 Sep 2015 020000 -0400)
%%O    latitude                (36.43333)
%%P    longitude               (-81.41667)
%%Q    location                (New York, Kennedy International Airport, NY)
%%R    station_id              (KJFK)
%%%%    a literal %%

Example:
    Display current temperature and wind direction\n
        $ pywnoaa KIJD -m "Current temperature is %%n, wind direction is %%r"\n
    Example output:\n
        Current temperature is 56.0F, wind direction is North''')

    return argparser.parse_args()
