import pywnoaa
import unittest
import sys


class PyweatherTestCase(unittest.TestCase):
    def test_noaa_weather(self):
        weather = pywnoaa.noaa_weather('KIJD')
        assert weather['station_id'] == 'KIJD'
        assert 'error' not in weather

        with self.assertRaises(pywnoaa.exceptions.ConnectionError):
            pywnoaa.noaa_weather('INVALID')

    def test_map_data(self):
        fields = {'%f': 'temp_f', '%c': 'temp_c', '%w': 'wind'}
        data = {'temp_f': '76', 'temp_c': '24'}
        mapping = pywnoaa.map_data(fields, data)
        assert mapping['%f'] == '76'
        assert mapping['%c'] == '24'
        assert mapping['%w'] is None

    def test_format_string(self):
        mapping = {'%f': '76', '%c': '24', '%w': None}
        format = '%fF %cC Wind: %w'
        string = pywnoaa.format_string(format, mapping)
        assert string == '76F 24C Wind: '

    def test_main(self):
        string = '%%a | %R'
        weather = pywnoaa.main(string, 'KIJD')
        assert weather.startswith('%a')
        assert '| KIJD' in weather

        weather = pywnoaa.main(string, 'INVALID', return_error=True)
        assert weather == 'Could not connect to NOAA'
        with self.assertRaises(pywnoaa.exceptions.ConnectionError):
            pywnoaa.main(string, 'INVALID', return_error=False)

if __name__ == '__main__':
    unittest.main()
