class ConnectionError(Exception):
    """A network error occurred connecting to NOAA."""
