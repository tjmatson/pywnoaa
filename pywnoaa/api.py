#!/usr/bin/env python

from .exceptions import ConnectionError
from .weather import noaa_weather
from .utils import NOAA_FIELDS

import re


def main(string, station, return_error=True):
    """Run the main program.

    :param string: string to format.
    :param station: station ICAO code.
    :param return_error: return NOAA errors instead of raising them. Defaults to True.
    :type return_error: bool
    :rtype: str
    """
    try:
        response = noaa_weather(station)
    except ConnectionError as e:
        if return_error:
            return e.args[0]
        else:
            raise e

    weather = map_data(NOAA_FIELDS, response)

    weather['%%'] = '%'  # Used for % escaping
    result = format_string(string, weather)

    return result


def map_data(fields, data):
    """Map placeholder values with data.

    Placeholders without corresponding data will be given a value of None.

    :param fields: dictionary containing placeholders and names of data.
    :param data: dictionary containing names of data and data itself.
    :rtype: dict

    Usage::

      >>> fields = {'%a': 'd1', '%b': 'd2', '%c': 'd3'}
      >>> data = {'d1': '10', 'd2': '50'}
      >>> map_data(fields, data)
      {'%a': '10', '%b': '50', '%c': None}

    """
    mapping = {}
    for k, v in fields.items():
        try:
            mapping[k] = data[v]
        except KeyError:
            mapping[k] = None

    return mapping


def format_string(string, mapping):
    """Format string using mapping.

    :param string: string to be formatted.
    :param mapping: dictionary with format data.
    :rtype: str

    Usage::

      >>> string = 'Format %a and %b'
      >>> mapping = {'%a': '10', '%b': '50'}
      >>> format_string(string, mapping)
      'Format 10 and 50'
    """
    patterns = '|'.join(mapping.keys())
    pattern = re.compile(r'({})'.format(patterns))
    return pattern.sub(lambda x: mapping[x.group()], string)
