from urllib.request import urlopen
from urllib.error import URLError
from xml.etree import ElementTree

from .exceptions import ConnectionError
from .utils import NOAA_FIELDS

NOAA_WEATHER_URL = 'http://www.weather.gov/xml/current_obs/{}.xml'


def noaa_weather(station):
    """Return NOAA weather data.

    :param station: NOAA station ID
    :type station: str
    :rtype: dict
    """
    url = NOAA_WEATHER_URL.format(station)

    try:
        response = urlopen(url)
    except URLError:
        raise ConnectionError('Could not connect to NOAA')

    root = ElementTree.fromstring(response.read())

    tags = list(NOAA_FIELDS.values())
    data = {}
    for tag in tags:
        try:
            data[tag] = root.find(tag).text
        except AttributeError:
            pass

    return data
